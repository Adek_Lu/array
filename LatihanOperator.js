var x = 4;
var y = 3;
var z = 0;

// penjumlahan
var z = x + y;
console.log(`${x} + ${y} = ${z}`);

//pengurangan
var z = x - y;
console.log(`${x} - ${y} = ${z}`);

//perkalian
var z = x * y;
console.log(`${x} * ${y} = ${z}`);

//pembagian
var z = x / y;
console.log(`${x} / ${y} = ${z}`);

//pemangkatan
var z = x ** y;
console.log(`${x} ** ${y} = ${z}`);

//hasil bagi
var z = x % y;
console.log(`${x} % ${y} = ${z}`);


//Operator Penugasan
console.log("Mula-mula nilai Score:");

//Pengisian nilai
var Score = 100;
console.log("Score =" + Score);

//pengisian dan penjumlahan dengan 10
Score += 10;
console.log("Score =" + Score);

//pengisian dan pengurangan dengan 5
Score -= 5;
console.log("Score =" + Score);

//pengisian dan perkalian dengan 5
Score *= 5;
console.log("Score =" + Score);

//pengisian dan pembagian dengan 5
Score /= 5;
console.log("Score =" + Score);

//pengisian dan pemangkatan dengan 2
Score **= 2;
console.log("Score =" + Score);

//pengisian dan hasil bagi dengan 3
Score %= 3;
console.log("Score =" + Score);
